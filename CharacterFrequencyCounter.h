/* 
 * File:   CharacterFrequencyCounter.h
 * Author: Darren Silke
 *
 * Created on 31 March 2015, 12:14 PM
 */

#ifndef CHARACTERFREQUENCYCOUNTER_H
#define	CHARACTERFREQUENCYCOUNTER_H

#include <unordered_map>

using std::istream;
using std::unordered_map;

namespace SLKDAR001
{
    // This class is used to count the number of occurrences of each letter in a
    // text file. To store these frequencies, a std::unordered_map with keys of
    // type character and values of integer are used.
    class CharacterFrequencyCounter
    {
    public:
        /*
         * Constructor with an open istream as a parameter. 
         */
        CharacterFrequencyCounter(istream & input);      
        /*
         * Returns the number of occurrences of character.
         */
        int getCharacterFrequency(char character) const;
        /*
         * Sets the number of occurrences of character.
         */
        void setCharacterFrequency(char character, int count);
    private:
        unordered_map<char, int> theCharacterFrequencies;
    };
}
#endif	/* CHARACTERFREQUENCYCOUNTER_H */
