/* 
 * File:   BitInputStream.cpp
 * Author: Darren Silke
 *
 * Created on 02 April 2015, 12:30 PM
 */

#include "BitInputStream.h"

using std::istream;

namespace SLKDAR001
{
    // Return bit at position pos in a packed set of bits (pack).
    int getBit(char pack, int pos)
    {
        return (pack & (1 << pos)) ? 1 : 0;
    }
    // Set bit at position pos in a packed set of bits (pack).
    void setBit(char & pack, int pos, int val)
    {
        if(val == 1)
        {
            pack |= (val << pos);
        }
    }  
    // Construct the bit-input stream.
    BitInputStream::BitInputStream(istream & is)        
    : bufferPos(BITS_PER_CHARACTER), in(is)
    {
        /*
         * Deliberately left empty.
         */
    }  
    // Read one bit.
    int BitInputStream::readBit()
    {
        if(bufferPos == BITS_PER_CHARACTER)
        {
            // No bits left in the buffer, so get a new set of bits for buffer.
            in.get(buffer);
            if(in.eof())
            {
                return EOF;
            }
            // Reset bufferPos.
            bufferPos = 0;
        }
        return getBit(buffer, bufferPos++);
    }
    // Return underlying input stream.
    istream & BitInputStream::getInputStream() const
    {
        return in;
    }
}
