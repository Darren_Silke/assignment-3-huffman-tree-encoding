/* 
 * File:   BitOutputStream.h
 * Author: Darren Silke
 *
 * Created on 02 April 2015, 1:36 PM
 */

#ifndef BITOUTPUTSTREAM_H
#define	BITOUTPUTSTREAM_H

#include <fstream>
#include <vector>

using std::ostream;
using std::vector;

namespace SLKDAR001
{
    // Bit-output stream wrapper class.
    class BitOutputStream
    {
    public:
        /*
         * Constructor with an open ostream as a parameter.
         */
        BitOutputStream(ostream & os);
        /*
         * Destructor which calls the flush() function to flush buffered bits.
         */
        ~BitOutputStream(); 
        /*
         * Write one bit (0 or 1).
         */
        void writeBit(int val);
        /*
         * Write a vector of bits.
         */
        void writeBits(const vector<int> & val);
        /*
         * Flush buffered bits.
         */
        void flush();
        /*
         * Return underlying stream.
         */
        ostream & getOutputStream() const;
        /*
         * Return the total number of bits.
         */
        int getNumberOfBits();
    private:
        // The underlying output stream.
        ostream & out; 
        // Buffer to store eight bits at a time.
        char buffer; 
        // Position in buffer for next write.
        int bufferPos; 
        // Number of bits in the compressed file.
        int numberOfBits; 
    };
}
#endif	/* BITOUTPUTSTREAM_H */
