/* 
 * File:   BitInputStream.h
 * Author: Darren Silke
 *
 * Created on 01 April 2015, 12:36 PM
 */

#ifndef BITINPUTSTREAM_H
#define	BITINPUTSTREAM_H

#include <fstream>

using std::istream;

namespace SLKDAR001
{    
    static const int NUMBER_OF_DIFFERENT_CHARACTERS = 256;
    static const int BITS_PER_CHARACTER = 8;
    
    /*
     * Function returns bit at position pos in a packed set of bits (pack). 
     */
    int getBit(char pack, int pos);
    /*
     * Function sets bit at position pos in a packed set of bits (pack).
     */
    void setBit(char & pack, int pos, int val);
    
    // Bit-input stream wrapper class.
    class BitInputStream
    {
    public:
        /*
         * Constructor which takes an open istream as a parameter.
         */
        BitInputStream(istream & is);        
        /*
         * Read one bit as a 0 or 1.
         */
        int readBit();
        /*
         * Return the underlying stream.
         */
        istream & getInputStream() const;
    private:
        // The underlying input stream.
        istream & in;
        // Buffer to store eight bits at a time.
        char buffer;
        // Position in buffer for next read.
        int bufferPos; 
    };    
}
#endif	/* BITINPUTSTREAM_H */
