/* 
 * File:   Main.cpp
 * Author: Darren Silke
 *
 * Created on 30 March 2015, 11:18 AM
 */

#include <iostream>

#include "Compressor.h"

using std::cerr;

using SLKDAR001::Compressor; 

/*
 * Simple main that performs compression using a Huffman encoding tree on an 
 * ASCII text file.
 */
int main(int argc, char* argv[]) 
{
    if(argc != 3)
    {
        cerr << "Invalid command line arguments!\n"
             << "Format is the following: "
             << "huffencode <input_file><output_file>\n"
             << "where input_file is an ASCII text file and output_file is the name of the\ncompressed file.\n";
        return 1;
    } 
    // UNCOMMENT THE LINE BELOW TO WRITE THE OUTPUT TO A FILE WITHOUT 
    // USING A BIT STREAM.
    // ENSURE THAT THE LINE BELOW IT IS THEN COMMENTED OUT.
    // Compressor::compressWithoutBitStream(argv[1], argv[2]);
    Compressor::compressWithBitStream(argv[1], argv[2]);
    return 0;
}
