/* 
 * File:   CharacterFrequencyCounter.cpp
 * Author: Darren Silke
 *
 * Created on 31 March 2015, 1:27 PM
 */

#include <fstream>

#include "CharacterFrequencyCounter.h"

namespace SLKDAR001
{
    // Constructor: Get character frequencies by reading from input stream.
    CharacterFrequencyCounter::CharacterFrequencyCounter(istream & input)
    {
        char character;
        while(!input.get(character).eof())
        {
            theCharacterFrequencies[character]++;
        }
    }
    // Return the character frequency for character.
    int CharacterFrequencyCounter::getCharacterFrequency(char character) const
    {
        unordered_map<char, int>::const_iterator itr;
        itr = theCharacterFrequencies.find(character);
        
        return itr != theCharacterFrequencies.end() ? (*itr).second : 0;
    }
    // Set the character frequency for character.
    void CharacterFrequencyCounter::setCharacterFrequency(char character, int count)
    {
        theCharacterFrequencies[character] = count;
    }   
}
