/* 
 * File:   Compressor.h
 * Author: Darren Silke
 *
 * Created on 02 April 2015, 6:51 PM
 */

#ifndef COMPRESSOR_H
#define	COMPRESSOR_H

#include <string>

#include "HuffmanTree.h"

using std::string;

namespace SLKDAR001
{
    class Compressor
    {
    public:  
        /*
         * Function compresses inputFile and writes the result to outputFile.
         * The output file name is formed by appending ".huf" to outputFile.
         * This function does not use a bit stream. Therefore, it is possible 
         * for the output file to be larger than the input file. The output file
         * is simply a text file with 0s and 1s to represent the compressed 
         * data.
         */
        static void compressWithoutBitStream(const string & inputFile, const string & outputFile);
        /*
         * Function compresses inputFile and writes the result to outputFile. 
         * The output file name is formed by appending ".huf" to outputFile. 
         * This function uses a bit stream. The output file is a binary file.
         * The output file should be smaller than the input file.
         */
        static void compressWithBitStream(const string & inputFile, const string & outputFile);
    private:
        /*
         * Function reads in the compressed file and unpacks the bit stream. The
         * function verifies that the unpacked bit stream matches the original
         * encoded data. A boolean is returned representing whether the 
         * compressed file does indeed match the original encoded data.
         */
        static bool verifyCompressedFile(const string & compressedFile, const string & encodedData, const HuffmanTree & huffmanTree);
    };
}
#endif	/* COMPRESSOR_H */
