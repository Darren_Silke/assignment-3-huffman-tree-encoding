/* 
 * File:   HuffmanTree.cpp
 * Author: Darren Silke
 *
 * Created on 31 March 2015, 3:42 PM
 */

#include <algorithm>
#include <queue>

#include "HuffmanTree.h"
#include "BitInputStream.h"
#include "SharedPointerWrapper.hpp"

using std::reverse;
using std::priority_queue;
using std::make_shared;
using std::less;
using std::endl;

namespace SLKDAR001
{
    // Construct the tree given a CharacterFrequencyCounter object.
    HuffmanTree::HuffmanTree(const CharacterFrequencyCounter & cfc)        
    : theCharacterFrequencies(cfc)
    {
        root = nullptr;
        buildTree();
    }
    // Destructor sets the root node to nullptr.
    HuffmanTree::~HuffmanTree()
    {
        root = nullptr;
    } 
    // Return the code corresponding to the character, character.
    // (The parameter is an int to accommodate EOF).
    // If the character code representation is not found, return a vector of 
    // size 0.
    vector<int> HuffmanTree::getCode(int character) const
    {
        unordered_map<int, shared_ptr<HuffmanNode>>::const_iterator itr;
        itr = theNodes.find(character);
        
        if(itr == theNodes.end())
        {
            return vector<int>();
        }
        shared_ptr<HuffmanNode> current = (*itr).second;
        
        vector<int> characterCodeRepresentation;
        shared_ptr<HuffmanNode> par = current->parent;
        
        while(par != nullptr)
        {
            if(par->leftChild == current)
            {
                // Current is a left child.
                characterCodeRepresentation.push_back(0);
            }
            else
            {
                // Current is a right child.
                characterCodeRepresentation.push_back(1);
            }
            current = current->parent;
            par = current->parent;
        }
        // Reverse is used, because the tree is traversed from the bottom to the
        // top. Therefore the character code representation that is retrieved is 
        // in the reverse order.
        reverse(characterCodeRepresentation.begin(), characterCodeRepresentation.end());
        return characterCodeRepresentation;      
    }
    // Get the character corresponding to the character code representation.
    int HuffmanTree::getCharacter(const vector<int> & characterCode) const
    {
        shared_ptr<HuffmanNode> pointerToRoot = root;
        
        for(int i = 0; pointerToRoot != nullptr && i < characterCode.size(); i++)
        {
            if(characterCode[i] == 0)
            {
                pointerToRoot = pointerToRoot->leftChild;
            }
            else
            {
                pointerToRoot = pointerToRoot->rightChild;
            }
        }
        if(pointerToRoot == nullptr)
        {
            return ERROR;
        }
        return pointerToRoot->getValue();
    }
    // Write a code table to an output stream.
    void HuffmanTree::writeCodeTable(ostream & out)
    {
        int totalNumberOfCharacters = 0;
        for(int i = 0; i < NUMBER_OF_DIFFERENT_CHARACTERS; i++)
        {
            if(theCharacterFrequencies.getCharacterFrequency(i) > 0)
            {
                totalNumberOfCharacters += theCharacterFrequencies.getCharacterFrequency(i);
            }
        } 
        out << "TOTAL NUMBER OF CHARACTERS IN CODE TABLE: " << totalNumberOfCharacters << endl << endl;
        for(int i = 0; i < NUMBER_OF_DIFFERENT_CHARACTERS; i++)
        {
            if(theCharacterFrequencies.getCharacterFrequency(i) > 0)
            {
                vector<int> characterCode = getCode(i);
                out << "Character: " << static_cast<char>(i) << endl
                    << "Code Representation: ";
                for(int i = 0; i < characterCode.size(); i++)
                {
                    out << characterCode[i];
                }
                out << endl 
                    << "Frequency: " << theCharacterFrequencies.getCharacterFrequency(i) << endl << endl;
            }
        }
    }
    // Comparison function for a HuffmanNode.
    // The meaning is reversed in order to ensure that the priority_queue will
    // retrieve the node with the minimum weight.
    bool operator < (const HuffmanNode & lhs, const HuffmanNode & rhs)
    {
        return lhs.weight > rhs.weight;
    }
    // Construct the Huffman coding tree.
    void HuffmanTree::buildTree()
    {
        priority_queue<SharedPointerWrapper<HuffmanNode>, vector<SharedPointerWrapper<HuffmanNode>>, less<SharedPointerWrapper<HuffmanNode>>> priorityQueue;
        
        for(int i = 0; i < NUMBER_OF_DIFFERENT_CHARACTERS; i++)
        {
            if(theCharacterFrequencies.getCharacterFrequency(i) > 0)
            {
                shared_ptr<HuffmanNode> newNode = make_shared<HuffmanNode>(i, theCharacterFrequencies.getCharacterFrequency(i), nullptr, nullptr, nullptr);
                theNodes[i] = newNode;
                priorityQueue.push(SharedPointerWrapper<HuffmanNode>(newNode));
            }
        }
        
        theNodes[END] = make_shared<HuffmanNode>(END, 1, nullptr, nullptr, nullptr);
        priorityQueue.push(SharedPointerWrapper<HuffmanNode>(theNodes[END]));
        
        while(priorityQueue.size() > 1)
        {
            shared_ptr<HuffmanNode> node1 = priorityQueue.top();
            priorityQueue.pop();
            
            shared_ptr<HuffmanNode> node2 = priorityQueue.top();
            priorityQueue.pop();
            
            shared_ptr<HuffmanNode> result = make_shared<HuffmanNode>(INCOMPLETE_CODE, node1->getWeight() + node2->getWeight(), node1, node2, nullptr);
            node1->parent = node2->parent = result;
            priorityQueue.push(SharedPointerWrapper<HuffmanNode>(result));
        }
        root = priorityQueue.top();
    }
}
