Author: Darren Silke
Date: 8 April 2015

Name: Assignment 3 - Huffman Tree Encoding

Description: This software implements a compression algorithm to compress English language text files. The compression algorithm makes use of a Huffman encoded tree.

DUE TO TIME CONSTRAINTS, NO UNIT TESTING WAS IMPLEMENTED.

Instructions:

1. Open terminal BASH.
2. Type 'make' to compile and link all C++ source files.
3. To run the software, see HOW TO RUN below.
4. Follow the instructions that appear on the screen (if any).
5. Evaluate output.
6. To remove the 'huffencode' executable and the .o files, type 'make clean'.

HOW TO RUN:

To run the application, make sure that any files needed by the application are in the same directory as the application. In this case, ensure that the input text file is in the same directory as the application.

To invoke the application on the terminal, the following format is required, as the application relies on command line arguments:

huffencode <input_file><output_file>

where 'huffencode' is the name of the executable, <input_file> is the name of the input text file and <output_file> is the name of the compressed output file.

BELOW IS AN EXAMPLE OF INVOKING THE APPLICATION USING AN INPUT TEXT FILE.

huffencode InputFile.txt OutputFile

List Of Files:

1. README.txt - Information file.
2. Makefile - Used to compile the program.
3. BitInputStream.h - Bit-input stream wrapper class.
4. BitInputStream.cpp - Implements the functions defined in BitInputStream.h
5. BitOutputStream.h - Bit-output stream wrapper class.
6. BitOutputStream.cpp - Implements the functions defined in BitOutputStream.h
7. CharacterFrequencyCounter.h - Used to count the number of occurrences of each letter in a text file.
8. CharacterFrequencyCounter.cpp - Implements the functions defined in CharacterFrequencyCounter.h
9. Compressor.h - Used to compress the input text file and write the result to an output file.
10. Compressor.cpp - Implements the functions defined in Compressor.h
11. HuffmanNode.h - Used to store a basic node in a Huffman coding tree.
12. HuffmanTree.h - Used to manipulate a Huffman coding tree.
13. HuffmanTree.cpp - Implements the functions defined in HuffmanTree.h
14. Main.cpp - Driver file that contains a simple main that performs compression using a Huffman encoding tree on an ASCII text file.
15. SharedPointerWrapper.hpp - Used to wrap a shared pointer variable for sorting.

TAKE NOTE OF THE .git FOLDER WHICH CONTAINS ALL INFORMATION RELATING TO THE USE OF GIT FOR A LOCAL REPOSITORY AS REQUIRED BY THIS COURSE.

Please refer to the assignment instructions for more information.
