# Makefile for compiling C++ source code.
# Darren Silke (SLKDAR001), 2015

CC=g++ # The compiler name.
CCFLAGS=-std=c++11 # Flags passed to compiler.

# The normal build rules.

huffencode: BitInputStream.o BitOutputStream.o CharacterFrequencyCounter.o Compressor.o HuffmanTree.o Main.o
	$(CC) $(CCFLAGS) BitInputStream.o BitOutputStream.o CharacterFrequencyCounter.o Compressor.o HuffmanTree.o Main.o -o huffencode

BitInputStream.o: BitInputStream.cpp BitInputStream.h
	$(CC) $(CCFLAGS) BitInputStream.cpp -c
	
BitOutputStream.o: BitOutputStream.cpp BitOutputStream.h
	$(CC) $(CCFLAGS) BitOutputStream.cpp -c

CharacterFrequencyCounter.o: CharacterFrequencyCounter.cpp CharacterFrequencyCounter.h
	$(CC) $(CCFLAGS) CharacterFrequencyCounter.cpp -c

Compressor.o: Compressor.cpp Compressor.h
	$(CC) $(CCFLAGS) Compressor.cpp -c

HuffmanTree.o: HuffmanTree.cpp HuffmanTree.h HuffmanNode.h SharedPointerWrapper.hpp
	$(CC) $(CCFLAGS) HuffmanTree.cpp -c

Main.o: Main.cpp
	$(CC) $(CCFLAGS) Main.cpp -c

# Clean rule.

clean:
	rm -f *.o huffencode
