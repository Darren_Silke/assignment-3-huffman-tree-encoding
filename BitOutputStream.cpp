/* 
 * File:   BitOutputStream.cpp
 * Author: Darren Silke
 *
 * Created on 02 April 2015, 1:56 PM
 */

#include "BitOutputStream.h"
#include "BitInputStream.h"

namespace SLKDAR001
{
    // Construct the bit-output stream.
    BitOutputStream::BitOutputStream(ostream & os)        
    : bufferPos(0), buffer(0), out(os), numberOfBits(0)
    {
        /*
         * Deliberately left empty.
         */
    }
    // Destructor (writes any bits left in the buffer).
    BitOutputStream::~BitOutputStream()
    {
        flush();
    }
    // If the buffer is non-empty, write one more char.
    void BitOutputStream::flush()
    {
        // If buffer is empty.
        if(bufferPos == 0) 
        {
            // Nothing to do.
            return; 
        }
        // Write the buffer.
        out.put(buffer); 
        // Reset the position.
        bufferPos = 0; 
        // Clear the buffer.
        buffer = 0; 
    }
    // Write one bit.
    void BitOutputStream::writeBit(int val)
    {
        setBit(buffer, bufferPos++, val);
        if(bufferPos == BITS_PER_CHARACTER)
        {
            flush();
        }
        numberOfBits++;
    }
    // Write a vector of bits.
    void BitOutputStream::writeBits(const vector<int> & val)
    {
        for(int i = 0; i < val.size(); i++)
        {
            writeBit(val[i]);
        }
    }
    // Return underlying output stream.
    ostream & BitOutputStream::getOutputStream() const
    {
        return out;
    }
    int BitOutputStream::getNumberOfBits()
    {
        return numberOfBits;
    }
}
