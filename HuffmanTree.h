/* 
 * File:   HuffmanTree.h
 * Author: Darren Silke
 *
 * Created on 31 March 2015, 2:39 PM
 */

#ifndef HUFFMANTREE_H
#define	HUFFMANTREE_H

#include <vector>
#include <fstream>
#include <memory>

#include "CharacterFrequencyCounter.h"
#include "HuffmanNode.h"

using std::vector;
using std::ostream;
using std::shared_ptr;

namespace SLKDAR001
{
    // Huffman tree class to manipulate a Huffman coding tree.
    class HuffmanTree
    {
    public:
        enum{ERROR = -3, INCOMPLETE_CODE = -2, END = -1};
        
        /*
         * Constructor with a CharacterFrequencyCounter object as a parameter.
         */
        HuffmanTree(const CharacterFrequencyCounter & cfc);
        /*
         * Destructor sets the root node to nullptr.
         */
        ~HuffmanTree();
        /*
         * Returns the code representation given a character.
         */
        vector<int> getCode(int character) const;
        /*
         * Returns a character given the code representation.
         */
        int getCharacter(const vector<int> & characterCode) const;     
        /*
         * Write code table to outStream using character frequencies.
         */
        void writeCodeTable(ostream & outStream);
    private:
        CharacterFrequencyCounter theCharacterFrequencies;
        unordered_map<int, shared_ptr<HuffmanNode>> theNodes;
        shared_ptr<HuffmanNode> root;
        
        void buildTree();
    };
}
#endif	/* HUFFMANTREE_H */
