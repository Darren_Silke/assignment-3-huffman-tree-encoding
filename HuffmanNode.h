/* 
 * File:   HuffmanNode.h
 * Author: Darren Silke
 *
 * Created on 31 March 2015, 10:56 AM
 */

#ifndef HUFFMANNODE_H
#define	HUFFMANNODE_H

#include <memory>

using std::shared_ptr;
using std::make_shared;
using std::move;

namespace SLKDAR001
{
    // Basic node in a Huffman coding tree.
    class HuffmanNode
    {
    public:
        shared_ptr<HuffmanNode> leftChild;
        shared_ptr<HuffmanNode> rightChild;
        shared_ptr<HuffmanNode> parent;
        
        /*
         * Constructor.
         */
        HuffmanNode(int v, int w, shared_ptr<HuffmanNode> ltChild, shared_ptr<HuffmanNode> rtChild, shared_ptr<HuffmanNode> pt)        
        : value(v), weight(w), leftChild(ltChild), rightChild(rtChild), parent(pt)
        {
            /*
             * Deliberately left empty.
             */
        }
        /*
         * Copy Constructor.
         */
        HuffmanNode(const HuffmanNode & rhs)
        : value(rhs.value), weight(rhs.weight), leftChild(rhs.leftChild), rightChild(rhs.rightChild), parent(rhs.parent)
        {
            /*
             * Deliberately left empty.
             */
        }
        /*
         * Move Constructor.
         */
        HuffmanNode(HuffmanNode && rhs)
        : value(move(rhs.value)), weight(move(rhs.weight)), leftChild(rhs.leftChild), rightChild(rhs.rightChild), parent(rhs.parent)
        {
            rhs.value = -1;
            rhs.weight = -1;
            rhs.leftChild = nullptr;
            rhs.rightChild = nullptr;
            rhs.parent = nullptr;
        }
        /*
         * Copy Assignment Operator.
         */
        HuffmanNode & operator = (const HuffmanNode & rhs)
        {
            if(this != &rhs)
            {
                // Release current resources.
                if((value != -1) && (weight != -1))
                {            
                    leftChild = nullptr;
                    rightChild = nullptr;
                    parent = nullptr;
                }
                // Copy data across.
                value = rhs.value;
                weight = rhs.weight;
                leftChild = rhs.leftChild;
                rightChild = rhs.rightChild;
                parent = rhs.parent; 
            }
            return *this;
        }    
        /*
         * Move Assignment Operator.
         */
        HuffmanNode & operator = (HuffmanNode && rhs)
        {
            if(this != &rhs)
            {
                // Release current resources.
                if((value != -1) && (weight != -1))
                {            
                    leftChild = nullptr;
                    rightChild = nullptr;
                    parent = nullptr;
                }
                // Steal resources/values from rhs!
                value = rhs.value;
                weight = rhs.weight;
                leftChild = rhs.leftChild;
                rightChild = rhs.rightChild;
                parent = rhs.parent;
                // Leave rhs in empty state.
                rhs.value = -1;
                rhs.weight = -1;
                rhs.leftChild = nullptr;
                rhs.rightChild = nullptr;
                rhs.parent = nullptr;        
            }
            return *this;
        }    
        /*
         * Destructor.
         * 
         * Cleans up memory by setting the left child, right child and parent to 
         * nullptr. The two int variables, value and weight, are automatic 
         * variables. Their memory is freed up automatically.
         */
        ~HuffmanNode()
        {
            if((value != -1) && (weight != -1))
            {            
                leftChild = nullptr;
                rightChild = nullptr;
                parent = nullptr;
            }
        }     
        int getValue()
        {
            return value;
        }
        int getWeight()
        {
            return weight;
        }
        /*
         * Comparison function for a HuffmanNode.
         * This function is defined in the HuffmanTree.cpp file. A detailed
         * explanation is provided there.
         */
        friend bool operator < (const HuffmanNode & lhs, const HuffmanNode & rhs);
    private:
        // An int is used instead of char to allow all characters and the EOF 
        // symbol.
        int value;
        int weight;
    };
}
#endif	/* HUFFMANNODE_H */
