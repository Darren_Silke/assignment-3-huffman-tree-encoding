/* 
 * File:   SharedPointerWrapper.hpp
 * Author: Darren Silke
 *
 * Created on 1 April 2015, 6:00 PM
 */

#ifndef SHAREDPOINTERWRAPPER_HPP
#define	SHAREDPOINTERWRAPPER_HPP

#include <memory>

using std::shared_ptr;

namespace SLKDAR001
{
    // Class that wraps a shared pointer variable for sorting.
    template <class Comparable>
    class SharedPointerWrapper
    {
        public:
            explicit SharedPointerWrapper(shared_ptr<Comparable> rhs = nullptr)
            : sharedPointee(rhs)
            {
                /*
                 * Deliberately left empty.
                 */
            }
            bool operator < (const SharedPointerWrapper & rhs) const
            {
                return *sharedPointee < *rhs.sharedPointee;
            }
            operator shared_ptr<Comparable>() const
            {
                return sharedPointee;
            }
            shared_ptr<Comparable> get() const
            {
                return sharedPointee;
            }
        private:
            shared_ptr<Comparable> sharedPointee;
    };
}
#endif	/* SHAREDPOINTERWRAPPER_HPP */
