/* 
 * File:   Compressor.cpp
 * Author: Darren Silke
 *
 * Created on 02 April 2015, 7:03 PM
 */

#include <fstream>
#include <iostream>
#include <sstream>
#include <cstdlib>

#include "Compressor.h"
#include "BitInputStream.h"
#include "BitOutputStream.h"

using std::ios;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::cerr;

namespace SLKDAR001
{
    // Compress inputFile and write the result to outputFile. The output file's
    // name is formed by appending ".huf" to outputFile. This function does not
    // use a bit stream.
    void Compressor::compressWithoutBitStream(const string & inputFile, const string & outputFile)
    {
        string compressedFile = outputFile + ".huf";
        string headerFile = "Code Table.hdr";
        ifstream inputFileInStream(inputFile.c_str(), ios::in | ios::binary);
        
        if(inputFileInStream.fail())
        {
            cerr << "Failed to open " << inputFile << "\nAborting!\n";
            exit(1);
        }
        
        // Create the CharacterFrequencyCounter which stores each character and
        // the associated frequency.
        CharacterFrequencyCounter characterCounter(inputFileInStream);
        // Create the Huffman encoding tree.
        HuffmanTree huffmanTree(characterCounter);
        
        // Write out the code table to a header file.
        ofstream headerFileOutStream(headerFile.c_str(), ios::out);
        huffmanTree.writeCodeTable(headerFileOutStream);
        headerFileOutStream.close();
        
        ofstream compressedFileOutStream(compressedFile.c_str(), ios::out);
        
        // Rewind the stream.
        inputFileInStream.clear();
        inputFileInStream.seekg(0, ios::beg);
        
        // Read each character from the input file.
        char character;
        while(inputFileInStream.get(character))
        {
            // Get the character code representation from the Huffman tree for 
            // the particular character.
            // Store the character code representation in a vector of integers.
            vector<int> characterCode = huffmanTree.getCode(character);
            // Loop through the vector of integers containing the character code
            // representation and write it to the output stream for the
            // compressed file.
            for(int i = 0; i < characterCode.size(); i++)
            {
                compressedFileOutStream << characterCode[i];
            }
        }
        inputFileInStream.close();
        compressedFileOutStream.close();
    }
    // Compress inputFile and write the result to outputFile. The output file's
    // name is formed by appending ".huf" to outputFile. This function uses a 
    // bit stream.
    void Compressor::compressWithBitStream(const string & inputFile, const string & outputFile)
    {
        string compressedFile = outputFile + ".huf";
        string headerFile1 = "Code Table.hdr";
        string headerFile2 = "Number Of Bits.hdr";
        ifstream inputFileInStream(inputFile.c_str(), ios::in | ios::binary);
        
        if(inputFileInStream.fail())
        {
            cerr << "Failed to open " << inputFile << "\nAborting!\n";
            exit(1);
        }
        
        // Create the CharacterFrequencyCounter which stores each character and
        // the associated frequency.
        CharacterFrequencyCounter characterCounter(inputFileInStream);
        // Create the Huffman encoding tree.
        HuffmanTree huffmanTree(characterCounter);
        
        // Write out the code table to a header file.
        ofstream headerFile1OutStream(headerFile1.c_str(), ios::out);
        huffmanTree.writeCodeTable(headerFile1OutStream);
        headerFile1OutStream.close();
        
        ofstream compressedFileOutStream(compressedFile.c_str(), ios::out | ios::binary);
        
        // Rewind the stream.
        inputFileInStream.clear();
        inputFileInStream.seekg(0, ios::beg);
        // Create the bit stream to write to.
        BitOutputStream bitStreamOutStream(compressedFileOutStream);
        
        // Read each character from the input file.
        char character;
        // Used to store the original encoded data.
        stringstream encodedData;
        while(inputFileInStream.get(character))
        {
            // Write bits to the output bit stream.
            bitStreamOutStream.writeBits(huffmanTree.getCode(character & 0xff));
            // Get the character code representation from the Huffman tree for 
            // the particular character.
            // Store the character code representation in a vector of integers.
            vector<int> characterCode = huffmanTree.getCode(static_cast<int>(character));
            // Loop through the vector of integers containing the character code
            // representation and write it to the stringstream called 
            // encodedData. This will be used to verify that the data being 
            // written to the compressed file will match the original encoded 
            // data. See the verifyCompressedFile function.
            for(int i = 0; i < characterCode.size(); i++)
            {
                encodedData << characterCode[i];
            }
        }
        inputFileInStream.close();
        // Write the end of file code to the output bit stream before flushing 
        // and closing it.
        bitStreamOutStream.writeBits(huffmanTree.getCode(EOF));
        bitStreamOutStream.flush();
        compressedFileOutStream.close();
        
        // Write the number of bits in the compressed file to a separate header
        // file.
        ofstream headerFile2OutStream(headerFile2.c_str(), ios::out);
        headerFile2OutStream << bitStreamOutStream.getNumberOfBits();
        headerFile2OutStream.close();
        
        // Verify that the data in the compressed file actually matches the 
        // original encoded data. If it doesn't, display an error message.
        if(verifyCompressedFile(compressedFile, encodedData.str(), huffmanTree) == false)
        {
            cerr << "The encoded data file has been read in and the bit stream has been unpacked.\n"
                 << "The unpacked bit stream does not match the original encoded data!\n";
        }      
    }
    // Verify that the data in the compressed file actually matches the original
    // encoded data.
    bool Compressor::verifyCompressedFile(const string & compressedFile, const string & encodedData, const HuffmanTree & huffmanTree)
    {        
        ifstream inputFileInStream(compressedFile.c_str(), ios::in | ios::binary);
        BitInputStream bitStreamInStream(inputFileInStream);
        vector<int> bits;
        int bit;
        int decodedData;
        stringstream compressedFileDataUnpacked;
        bool dataEncodedCorrectly = false;
        
        while(!inputFileInStream.eof())
        {
            bit = bitStreamInStream.readBit();
            bits.push_back(bit);
            decodedData = huffmanTree.getCharacter(bits);
            if(decodedData == HuffmanTree::INCOMPLETE_CODE)
            {
                continue;
            }
            else if(decodedData == HuffmanTree::ERROR)
            {
                cerr << "Error decoding data!\n";
                break;
            }
            else if(decodedData == HuffmanTree::END)
            {
                break;
            }
            else
            {
                for(int i = 0; i < bits.size(); i++)
                {
                    compressedFileDataUnpacked << bits[i];
                }
                bits.resize(0);
            }
        }
        inputFileInStream.close();
        
        if(encodedData == compressedFileDataUnpacked.str())
        {
            dataEncodedCorrectly = true;
        }
        return dataEncodedCorrectly;
    }
}
